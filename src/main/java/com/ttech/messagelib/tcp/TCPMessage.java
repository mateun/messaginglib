package com.ttech.messagelib.tcp;

import java.nio.ByteBuffer;

public class TCPMessage implements Message {
	
	ByteBuffer body = null;

	public ByteBuffer getBody() {
		return body;
	}

	public void setBody(ByteBuffer messageBody) {
		body = messageBody;
		
	}

}
