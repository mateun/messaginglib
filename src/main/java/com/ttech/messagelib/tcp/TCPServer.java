package com.ttech.messagelib.tcp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

public class TCPServer {
	
	Thread serverThread = null;
	boolean shouldRun = true;
	List<Consumer<TCPMessage>> messageListeners = null;
	private Selector selector;
	ByteBuffer buffer = null;
	ByteBuffer writeBuffer = null;
	private ServerSocketChannel serverSocketChannel;
	List<TCPMessage> sendBuffer;
	
	public TCPServer() {
		messageListeners = new ArrayList<>();
		buffer = ByteBuffer.allocate(256);
		writeBuffer = ByteBuffer.allocate(4);
		sendBuffer = new ArrayList<>();
	}
	
	public void addMessageToOutBuffer(TCPMessage msg) {
		sendBuffer.add(msg);
	}
	
	public void flushSendBuffer() {
		try {
			selector.select();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		Set<SelectionKey> selectedKeys = selector.selectedKeys();
		Iterator<SelectionKey> iter = selectedKeys.iterator();
		while(iter.hasNext()) {
			SelectionKey key = iter.next();
			if (key.isWritable() & !sendBuffer.isEmpty()) {
				
				SocketChannel client = (SocketChannel) key.channel();
				sendBuffer.stream().forEach(m -> {
					try {
						client.write(m.body);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					m.body.flip();
				});
				
					
			}
			
			iter.remove();
		}
		
		sendBuffer.clear();
	}
	
	public void service() {
		try {
			selector.select();
			Set<SelectionKey> selectedKeys = selector.selectedKeys();
			Iterator<SelectionKey> iter = selectedKeys.iterator();
			
			
			while(iter.hasNext()) {
				
					SelectionKey key = iter.next();
					if (key.isAcceptable()) {
						SocketChannel client = serverSocketChannel.accept();
						client.configureBlocking(false);
						client.register(selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE);
					}
					
					if (key.isReadable()) {
						SocketChannel client = (SocketChannel) key.channel();
						InetSocketAddress address = (InetSocketAddress) client.getRemoteAddress();
						int clientPort = address.getPort();
						System.out.println("clientPort: " + clientPort);
						
						int result = client.read(buffer);
						if (result == -1) {
							client.close();
							System.out.println("a client disconnected!");
							key.cancel();
							continue;
							
						} else {
							System.out.println("message from client: " + new String(buffer.array()));
							System.out.println("nr. of bytes received: " + result);
							updateMessageListeners(buffer);
						
							
							Arrays.fill(buffer.array(), (byte)0);
							buffer.clear();
						}
					}
					
					iter.remove();
				
			}
		
		} catch(IOException ioex) {
			ioex.printStackTrace();
		} catch(CancelledKeyException ex) {
			ex.printStackTrace();
		}
	}


	

	public void start(int port) {
		
		System.out.println("Server starting.");
		
		
//				try {
//					ServerSocket serverSocket = new ServerSocket(port);
//					while(shouldRun) {
//						Socket clientSocket = serverSocket.accept();
//						BufferedReader bufferedReader = new BufferedReader(
//								new InputStreamReader(clientSocket.getInputStream()));
//						String clientRaw = bufferedReader.readLine();
//						System.out.println("clientMessage: " + clientRaw);
//						
//						TCPMessage clientMessage = new TCPMessage();
//						clientMessage.setBody(clientRaw);
//						
//						messageListeners.forEach(c -> c.accept(clientMessage));
//					}
//					
//					serverSocket.close();
//					
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//	
//
		
		
			try {
				selector = Selector.open();
				serverSocketChannel = ServerSocketChannel.open();
				serverSocketChannel.bind(new InetSocketAddress(port));
				serverSocketChannel.configureBlocking(false);
				serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
					
	}

	private void updateMessageListeners(ByteBuffer buffer) {
		TCPMessage clientMessage = new TCPMessage();
		buffer.rewind();
		clientMessage.setBody(buffer);
		messageListeners.forEach(c -> c.accept(clientMessage));
	}
	public void stop() {
		shouldRun = false;
	}
	
	public void addMessageListener(Consumer<TCPMessage> messageListener) {
		messageListeners.add(messageListener);
	}
	
}
