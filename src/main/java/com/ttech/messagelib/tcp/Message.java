package com.ttech.messagelib.tcp;

import java.nio.ByteBuffer;

public interface Message {
	
	ByteBuffer getBody();
	void setBody(ByteBuffer messageBody);

}
