package com.ttech.messagelib;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ttech.messagelib.tcp.Message;
import com.ttech.messagelib.tcp.TCPMessage;
import com.ttech.messagelib.tcp.TCPServer;

public class PubSubServer {
	
	private TCPServer tcpServer;
	private int port;
	private Map<String, List<Message>> topics;
	private Map<String, List<MessageClient>> subscriptions;
	private TCPMessage nextMessage = null;
	private int msgCounter = 0;
	
	public PubSubServer(int port) {
		this.port = port;
		tcpServer = new TCPServer();
		tcpServer.addMessageListener(this::handleMessage);
		topics = new HashMap<String, List<Message>>();
		subscriptions = new HashMap<String, List<MessageClient>>();
	}
	
	void service() {
		tcpServer.service();
		tcpServer.flushSendBuffer();

	}
	
	void handleMessage(Message msg) {
		System.out.println("received message in pubsub: " + msg.getBody() + " " + new String(msg.getBody().array()));
		nextMessage = new TCPMessage();
		ByteBuffer body = ByteBuffer.allocate(100);
		body.put("msg nr.:".getBytes());
		body.putInt((++msgCounter));
		body.flip();
		nextMessage.setBody(body);
		tcpServer.addMessageToOutBuffer(nextMessage);
		
	}
	
	public void start() {
		tcpServer.start(port);
	}
	
	public void createTopic(String name) {
		topics.put(name, new ArrayList<Message>());
	}
	
	public void publish(String topicName, Message msg) {
		topics.get(topicName).add(msg);
		
	}
	
	public void subscribe(String topicName, MessageClient client) {
		subscriptions.get(topicName).add(client);
	}

}
