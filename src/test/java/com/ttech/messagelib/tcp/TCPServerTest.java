package com.ttech.messagelib.tcp;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class TCPServerTest {
	
	
	@Test
	public void testMessageListener() {
		TCPServer tcpServer = new TCPServer();
		tcpServer.addMessageListener(body -> {
			assertNotNull(body);
			System.out.println("body received!" + body);
		});
		
		tcpServer.start(2000);
		tcpServer.stop();
	}

}
